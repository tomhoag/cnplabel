# CnPLabel

[![CI Status](http://img.shields.io/travis/Tom/CnPLabel.svg?style=flat)](https://travis-ci.org/Tom/CnPLabel)
[![Version](https://img.shields.io/cocoapods/v/CnPLabel.svg?style=flat)](http://cocoadocs.org/docsets/CnPLabel)
[![License](https://img.shields.io/cocoapods/l/CnPLabel.svg?style=flat)](http://cocoadocs.org/docsets/CnPLabel)
[![Platform](https://img.shields.io/cocoapods/p/CnPLabel.svg?style=flat)](http://cocoadocs.org/docsets/CnPLabel)

![iOS Simulator Screen Shot Mar 8, 2015, 10.48.17 AM.png](https://bitbucket.org/repo/Ke4Xy8/images/3551168274-iOS%20Simulator%20Screen%20Shot%20Mar%208,%202015,%2010.48.17%20AM.png)
![iOS Simulator Screen Shot Mar 8, 2015, 10.48.21 AM.png](https://bitbucket.org/repo/Ke4Xy8/images/3669105648-iOS%20Simulator%20Screen%20Shot%20Mar%208,%202015,%2010.48.21%20AM.png)
![iOS Simulator Screen Shot Mar 8, 2015, 10.48.27 AM.png](https://bitbucket.org/repo/Ke4Xy8/images/479866282-iOS%20Simulator%20Screen%20Shot%20Mar%208,%202015,%2010.48.27%20AM.png)

## Notes

There is a problem highlighting multiline labels.

## Usage

Using the interface builder, drop a UILabel onto a view and change its class type in the Inspector to CnPLabel.  If desired, attach an IBOutlet reference and a delegate reference to the CnPLabel.  Adding an IBOutlet reference is required if you'd like to alter any of the CnPLabel default property values:

### Properties
`(BOOL) canPaste` YES if the instance can be used as a target for pasting. (Default YES)

`(BOOL) canCopy` YES if the instance can be used as a source for copying. (Default YES)

`(UIColor *)selectionColor` The color to be used to highlight the text in the label.  (Default:
 <a nice coral blue>)

###Delegate
The delegate reference must adopt the `CnPLabelDelegate` protocol.  There are three optional methods in the protocol:

```
#!objective-c

- (BOOL)label:(CnPLabel *)label canPaste:(NSString *)string;
```
If implemented, this method is called on the delegate when the user selects a label.  Return YES if string can be pasted into label.  Return NO otherwise.  Note, this method will NOT be called if the `canPaste` property is set to NO.

This method is useful for validating a string as acceptable for the label -- i.e. A label used to hold number values would return YES only if string is a representation of a number.


```
#!objective-c

- (void) label:(CnPLabel *)label paste:(NSString *) string;
```

If implemented, this method is called on the delegate when the user touches the 'Paste' option on the pop up menu.

label: The CnPLabel that received the 'Paste' message

string: The text that was going to be pasted into the label

Use this method to format the string before pasting.  Usually, you will end this method call by putting a string into the label's text property.

If this delegate method is not implemented, `[UIPasteboard generalPasteboard].string` will be pasted into the label. 

```
#!objective-c

- (void)label:(CnPLabel *)label copyToPasteboard:(UIPasteboard *)pasteboard;
```
If implemented, this method is called on the delegate when the user touches the 'Copy' option on the pop up menu.

label: The CnPLabel that received the 'Paste' message

string: The UIPasteboard that the label was going to copy to

Use this method to format the string before copying.  Usually, you will end this method call by putting a string into the pasteboard's string property.

If this delegate method is not implemented, the text value of the label will be copied to `[UIPasteboard generalPasteboard].string` . 



## Requirements

## Installation

CnPLabel is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "CnPLabel"

## Author

Tom, tomhoag@Gmail.com

## License

CnPLabel is available under the MIT license. See the LICENSE file for more info.