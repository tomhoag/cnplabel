Pod::Spec.new do |s|
  s.name             = "CnPLabel"
  s.version          = "0.1.2"
  s.summary          = "A UILabel subclass with cut and paste functionality."
  s.description      = <<-DESC
A UILabel subclass with cut and paste functionality using the general UIPasteboard and UIMenuController.  CnPLabel can be customized with selection color and setup to be targets for cut or paste or both.  Further, using the CnPLabelDelegate protocol, a delegate can be used to validate and format the NSString to be pasted into the label.                       
DESC
  s.homepage         = "https://bitbucket.org/tomhoag/cnplabel"
  s.license          = 'MIT'
  s.author           = { "Tom" => "tomhoag@Gmail.com" }
  s.source           = { :git => "https://bitbucket.org/tomhoag/cnplabel.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'CnPLabel' => ['Pod/Assets/*.png']
  }
  s.frameworks = 'UIKit'
end
