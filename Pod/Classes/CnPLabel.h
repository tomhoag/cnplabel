
#import <UIKit/UIKit.h>

@class CnPLabel;

/**
 Delegate protocol that allows modification of default behavior of the cut and paste behavior
 */
@protocol CnPLabelDelegate <NSObject>
@optional

/**
 Override the default paste behavior of the CnPLabel
 
 Delegates that implement this method are essentially overriding the default paste
 behavior.
 
 Usually, you will end this method call with label.text = string;
 
 @param label The label that will be pasted to
 @param string The string that will be pasted into the labels text property
 @return void
 */

- (void)label:(CnPLabel *)label paste:(NSString *)string;

/**
 Override the default copy behavior of the CnPLabel
 
 Delegates that implement this method are essentially overriding the default copy
 behavior. Use this method to format the string before pasting.
 
 Usually, you will end this method call by putting a string into the label's text
 property.
 
 If this delegate method is not implemented, the label's text property will be set
 to [UIPasteboard generalPasteboard].string
 
 @param label The label that will be copied from
 @param pasteboard The pasteboard that will be copied to
 @return void
 */
- (void)label:(CnPLabel *)label copyToPasteboard:(UIPasteboard *)pasteboard;


/**
 If implemented, this method is called on the delegate when the user selects a label. Note, this method will NOT be called if the canPaste property is set to NO.
 
 While the canPaste property is a general on/off switch for pasting to this label, this method is
 useful for validating a string as acceptable for the label -- i.e. A label used to hold number
 values would return YES only if string is a representation of a number.
 
 @param label The CnPLabel that received the 'Paste' message
 @param string  The string to be pasted into the label
 @return Return YES if string can be pasted into label. Return NO otherwise.
 
 */
- (BOOL)label:(CnPLabel *)label canPerformPasteUsing:(NSString *)string;

/**
 If implemented, this method is called on the delegate when the user selects a label. You should return the range of self.string that should be highlighted.  If this method is not implemented, the entire string will be highlighted
 */
- (NSRange)rangeToBeHighlightedInLabel:(CnPLabel *)label;

@end

@interface CnPLabel : UILabel {}

/**
 * The delegate
 */
@property (nonatomic, strong) id <CnPLabelDelegate> delegate;

/**
 Property that determines the instances availability as a source for copying. (Default YES)
 */
@property (nonatomic) BOOL canCopy;

/**
 Property that determines the instances availability as a destination for pasting. (Default YES)
 */
@property (nonatomic) BOOL canPaste;

/**
 The color to be used to highlight the text in the label.  (Default <a nice coral blue>)
 */
@property (nonatomic, strong) UIColor *selectionColor;


@end
