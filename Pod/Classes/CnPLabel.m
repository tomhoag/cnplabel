#import "CnPLabel.h"
#import <CoreText/CoreText.h>

#define kRoundedRectangelBackgroundColor @"RoundedRectangleBackgroundColor"

@interface CnPLabel()

@property (nonatomic, strong) NSAttributedString *originalAttributedString;
@property (nonatomic) BOOL actionIsPaste;

@end

@implementation CnPLabel

#pragma mark Initialization

- (id) initWithFrame: (CGRect) frame {
    self = [super initWithFrame:frame];
    [self addLongTouchHandler];
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    [self addLongTouchHandler];
}

- (void) addLongTouchHandler {
    [self setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTouch = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self addGestureRecognizer:longTouch];
    
    _canCopy = YES;
    _canPaste = YES;
    _selectionColor = [UIColor colorWithRed:0xD1/255.f green:0xEE/255.f blue:0xFC/255.f alpha:1.0];
    
    _actionIsPaste = NO;
}

#pragma mark Clipboard

- (void) copy: (id) sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    if(_delegate && [_delegate respondsToSelector:@selector(label:copyToPasteboard:)]) {
        [_delegate label:self copyToPasteboard:pasteboard];
    } else {
        pasteboard.string = self.text;
    }
}

- (void) paste:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *pasteMe = pasteboard.string;
    
    if (_delegate && [_delegate respondsToSelector:@selector(label:paste:)]) {
        [_delegate label:self paste:pasteMe];
    } else {
        self.text = pasteMe;
    }
    _actionIsPaste = YES;
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    BOOL returnValue = NO; // default
    
    if(action == @selector(copy:)) {
        returnValue = _canCopy;
    }
    
    if(action == @selector(paste:)) {
        
        returnValue = _canPaste;
        
        if(_canPaste && _delegate && [_delegate respondsToSelector:@selector(label:canPerformPasteUsing:)]) {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            returnValue = [_delegate label:self canPerformPasteUsing:pasteboard.string];
        }
        
    }
    return returnValue;
}

#pragma mark -
#pragma mark Callbacks

- (void) handleGesture: (UIGestureRecognizer*) recognizer {
    
    if(recognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    [self becomeFirstResponder];
    
    [self addSelectionBackground];
    [self setNeedsDisplay];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    [menu setTargetRect:[self containingRect] inView:self];
    
    [menu setMenuVisible:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideMenu:) name:UIMenuControllerDidHideMenuNotification object:nil];
    
}

-(void)didHideMenu:(NSNotification *)aNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerDidHideMenuNotification object:nil];
    [self removeSelectionBackground];
    _actionIsPaste = NO;
}

#pragma mark -


- (BOOL) canBecomeFirstResponder {
    return self.isEnabled;
}

#pragma mark -
#pragma mark Add/Remove Selection Background

-(void)addSelectionBackground {
    
    if(self.attributedText == nil) {
        _originalAttributedString = nil;
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
        self.attributedText = attributedText;
    } else {
        _originalAttributedString = self.attributedText;
    }
    
    // minimally, we need self.attributedText to have an alignment and font attribute
    BOOL __block hasFontAttribute = NO;
    BOOL __block hasAlignmentAttribute = NO;
    
    [self.attributedText enumerateAttributesInRange:NSMakeRange(0, self.attributedText.length) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        
        [attrs enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            if([key isEqualToString:@"NSFont"]) {
                hasFontAttribute = YES;
            }
            if([key isEqualToString:@"NSParagraphStyle"]) {
                hasAlignmentAttribute = YES;
            }
        }];;
    }];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    
    if(!hasAlignmentAttribute) {
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.alignment = self.textAlignment;
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.text.length)];
    }
    
    if(!hasFontAttribute) {
        [attributedString addAttribute:NSFontAttributeName value:self.font range:NSMakeRange(0, self.text.length)];
    }
    
    // now add the kRoundedRectangelBackgroundColor attribute
    NSRange highlightRange = NSMakeRange(0,self.text.length);
    if(_delegate && [_delegate respondsToSelector:@selector(rangeToBeHighlightedInLabel:)]) {
        NSRange range = [_delegate rangeToBeHighlightedInLabel:self];
        highlightRange.location = range.location;
        highlightRange.length = range.length;
    }
    
    [attributedString addAttribute:kRoundedRectangelBackgroundColor value:_selectionColor range:highlightRange];
    
    self.attributedText = attributedString;
}

-(void)removeSelectionBackground {
    
    // If the current selection is a result of a paste operation, don't change the text in self
    if(_actionIsPaste)
        return;
    
    if(!_originalAttributedString) {
        self.text = self.attributedText.string;
        _originalAttributedString = nil;
    } else {
        self.attributedText = _originalAttributedString;
        _originalAttributedString = nil;
    }
}

-(void) drawRect:(CGRect)rect {
    
    CFAttributedStringRef asRef = (__bridge CFAttributedStringRef)self.attributedText;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(asRef);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, self.bounds );
    
    CFRange range = CFRangeMake(0, [self.attributedText length]);
    
    CTFrameRef totalFrame = CTFramesetterCreateFrame(framesetter, range, path, NULL);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    NSArray *lines = (NSArray *)CTFrameGetLines(totalFrame);
    CGPoint origins[lines.count];
    
    CTFrameGetLineOrigins(totalFrame, CFRangeMake(0, 0), origins);
    
    BOOL firstEncounter = YES;
    CFRange startRange = CFRangeMake(0, 0);
    CGFloat endIndex = 0;
    
    for(int lineIndex=0; lineIndex<lines.count;lineIndex++) {
        
        CTLineRef line = (__bridge CTLineRef)(lines[lineIndex]);
        
        CGContextSetTextPosition(context, origins[lineIndex].x, origins[lineIndex].y);
        
        NSArray *glyphRuns = (NSArray *)CTLineGetGlyphRuns(line);
        
        for(int glyphRunIndex=0;glyphRunIndex<glyphRuns.count;glyphRunIndex++) {
            
            CTRunRef run = (__bridge CTRunRef)(glyphRuns[glyphRunIndex]);
            
            NSDictionary *attributes = (NSDictionary *)CTRunGetAttributes(run);
            if([attributes objectForKey:kRoundedRectangelBackgroundColor]) {
                
                //CFRange runRange = CTRunGetStringRange(run);
                
                CGFloat ascent, descent, leading;
                CGRect runBounds = CGRectZero;
                runBounds.size.width = CTRunGetTypographicBounds(run,  CFRangeMake(0, 0), &ascent, &descent, &leading);
                runBounds.size.height = ascent + descent;
                runBounds.origin.x = CTLineGetOffsetForStringIndex(line, CTRunGetStringRange(run).location, NULL);
                
                // Not self.frame.size.height but rather the total height of all lines
                runBounds.origin.y = self.frame.size.height - origins[lines.count - lineIndex].y - runBounds.size.height;// - descent/2.f;
                
                CGSize radii = CGSizeMake(runBounds.size.height*0.25, runBounds.size.height*0.25);
                UIColor *selectionColor = (UIColor *)[attributes objectForKey:kRoundedRectangelBackgroundColor];
                
                // first line, round the LH top and bottom; last line, round the RH top and bottom
                UIRectCorner corners = 0;
                if(firstEncounter) {
                    corners = UIRectCornerTopLeft | UIRectCornerBottomLeft;
                    startRange = CTRunGetStringRange(run);
                    
                    // Get the range of the attributes
                    NSRange effectiveRange;
                    [self.attributedText attributesAtIndex:(startRange.location) longestEffectiveRange:&effectiveRange inRange:NSMakeRange(startRange.location, self.text.length-startRange.location)];
                    
                    // note the endIndex of the attribute
                    endIndex = effectiveRange.location + effectiveRange.length;
                    
                    firstEncounter = NO;
                }
                
                if(!firstEncounter) {
                    CFRange thisRange = CTRunGetStringRange(run);
                    if((thisRange.location+thisRange.length) >= endIndex) {
                        corners =  corners | UIRectCornerTopRight | UIRectCornerBottomRight;
                        firstEncounter = YES;
                    }
                }
                
                // Adjust for text alignment, if any
                CGFloat xAdj = 0.f;
                if([attributes objectForKey:NSParagraphStyleAttributeName]) {
                    NSParagraphStyle *ps = (NSParagraphStyle *)[attributes objectForKey:NSParagraphStyleAttributeName];
                    switch (ps.alignment) {
                        case NSTextAlignmentCenter:
                            xAdj = (self.frame.size.width - CTLineGetTypographicBounds(line, NULL, NULL, NULL))/2;
                            break;
                        case NSTextAlignmentRight:
                            xAdj = self.frame.size.width - CTLineGetTypographicBounds(line, NULL, NULL, NULL);
                            break;
                        case NSTextAlignmentJustified:
                        case NSTextAlignmentNatural:
                        case NSTextAlignmentLeft:
                        default:
                            break;
                    }
                }
                runBounds.origin.x += xAdj;
                
                UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:runBounds byRoundingCorners:corners cornerRadii:radii];
                [selectionColor setFill];
                [path fill];
                
            }
        }
    }
    
    CFRelease(path);
    CFRelease(totalFrame);
    CFRelease(framesetter);
    
    [super drawRect:rect];
}

-(CGRect) containingRect {
    CGRect boundingRect = CGRectZero;

    CFAttributedStringRef asRef = (__bridge CFAttributedStringRef)self.attributedText;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(asRef);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, self.bounds );
    
    CFRange range = CFRangeMake(0, [self.attributedText length]);
    
    CTFrameRef totalFrame = CTFramesetterCreateFrame(framesetter, range, path, NULL);
    
    UIGraphicsBeginImageContext(self.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetTextMatrix(context, CGAffineTransformIdentity);
    CGContextTranslateCTM(context, 0, self.bounds.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    NSArray *lines = (NSArray *)CTFrameGetLines(totalFrame);
    CGPoint origins[lines.count];
    
    CTFrameGetLineOrigins(totalFrame, CFRangeMake(0, 0), origins);
    
    for(int lineIndex=0; lineIndex<lines.count;lineIndex++) {
        
        CTLineRef line = (__bridge CTLineRef)(lines[lineIndex]);
        
        CGContextSetTextPosition(context, origins[lineIndex].x, origins[lineIndex].y);
        
        NSArray *glyphRuns = (NSArray *)CTLineGetGlyphRuns(line);
        
        
        for(int glyphRunIndex=0;glyphRunIndex<glyphRuns.count;glyphRunIndex++) {
            
            CTRunRef run = (__bridge CTRunRef)(glyphRuns[glyphRunIndex]);
            
            NSDictionary *attributes = (NSDictionary *)CTRunGetAttributes(run);
            if([attributes objectForKey:kRoundedRectangelBackgroundColor]) {
                
                //CFRange runRange = CTRunGetStringRange(run);
                
                CGFloat ascent, descent, leading;
                CGRect runBounds = CGRectZero;
                runBounds.size.width = CTRunGetTypographicBounds(run,  CFRangeMake(0, 0), &ascent, &descent, &leading);
                runBounds.size.height = ascent + descent;
                runBounds.origin.x = CTLineGetOffsetForStringIndex(line, CTRunGetStringRange(run).location, NULL);
                
                // Not self.frame.size.height but rather the total height of all lines
                runBounds.origin.y = self.frame.size.height - origins[lines.count - lineIndex].y - runBounds.size.height;// - descent/2.f;
                
                // Adjust for text alignment, if any
                CGFloat xAdj = 0.f;
                if([attributes objectForKey:NSParagraphStyleAttributeName]) {
                    NSParagraphStyle *paragraphStyle = (NSParagraphStyle *)[attributes objectForKey:NSParagraphStyleAttributeName];
                    switch (paragraphStyle.alignment) {
                        case NSTextAlignmentCenter:
                            xAdj = (self.frame.size.width - CTLineGetTypographicBounds(line, NULL, NULL, NULL))/2;
                            break;
                        case NSTextAlignmentRight:
                            xAdj = self.frame.size.width - CTLineGetTypographicBounds(line, NULL, NULL, NULL);
                            break;
                        case NSTextAlignmentJustified:
                        case NSTextAlignmentNatural:
                        case NSTextAlignmentLeft:
                        default:
                            break;
                    }
                }
                runBounds.origin.x += xAdj;
                
                if(CGRectIsEmpty(boundingRect)) {
                    boundingRect = runBounds;
                } else {
                    boundingRect = CGRectUnion(boundingRect, runBounds);
                }
                
            }
        }
    }
    
    CFRelease(path);
    CFRelease(totalFrame);
    CFRelease(framesetter);
    
    return boundingRect;

}

@end
